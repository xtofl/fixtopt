#!/usr/bin/env python3
import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

def ci_version(base_version):
    import os
    local_version = os.getenv("CI_PIPELINE_ID", None)
    if local_version is None:
        return base_version
    else:
        return  "{}+b{}".format(base_version, local_version)

setuptools.setup(
    name="fixtopt-xtofl",
    version=ci_version("0.1.dev3"),
    author="xtofl",
    author_email="xtofl@fixtopt.com",
    description="Pytest extension for adding test options",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/xtofl/fixtopt",
    package_dir = { 'fixtopt' : 'src' },
    packages=['fixtopt'],
    install_requires=['pytest'],
    tests_require=['pytest', 'pipenv'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "Topic :: Software Development :: Libraries :: Python Modules",
        "Topic :: Software Development :: Testing",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)