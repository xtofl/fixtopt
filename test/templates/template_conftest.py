"""template file for testing the mop pytest options as fixture"""

from pathlib import Path
from fixtopt import Option
from fixtopt import register

SCRIPTDIR = Path(__file__).absolute().parent


def pytest_addoption(parser):
    """declare options to be used when invoking pytest"""

    register(globals(), parser, (
        Option(name="x", default="defaultx", help="option x"),
        Option(name="y", default="defaulty", help="option y"),
        Option(name="option1", default="default1", help="option 1"),
        Option(name="message", default="Hello {}", help="the message"),
        Option(name="receiver", default="World", help="the receiver"),
    ))
