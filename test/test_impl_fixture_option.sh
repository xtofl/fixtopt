#!/bin/bash
set -euxo pipefail
PROJECT_DIR=$(realpath $(dirname $0)/..)

(
    TEST_DIR=$(mktemp -d)
    cd $PROJECT_DIR/test/templates
    for file in $(ls); do
        cp $file $TEST_DIR/${file/template_/}
    done

    (
        cd $TEST_DIR

        set -x
        python -m pytest -l test_options.py --option1=user_provided || exit ${LINENO}

        cat > pytest.ini <<EOF
[pytest]
addopts = --option1 user_provided
EOF
        python -m pytest test_options.py || exit ${LINENO}

        cat > pytest.ini <<EOF
[pytest]
addopts = --option1 config_provided
EOF
        python -m pytest test_options.py --option1=user_provided || exit ${LINENO}
        python -m pytest test_options.py && exit ${LINENO}
        python -m pytest test_options.py --option1=should_fail && exit ${LINENO}

        echo -e "\e[1m\e[32mTESTS SUCCEEDED\e[0m"
    ) || exit $?
) || (echo -e "\e[1;0;31mFAILURE on line $?\e[0m" && exit 1) || exit 1