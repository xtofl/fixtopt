#!/bin/bash

# make sure we can run the tests from whereever the
# current working directory is
cd $(dirname $0)

export PYTHONPATH=$(realpath ../src)

./test_impl_fixture_option.sh